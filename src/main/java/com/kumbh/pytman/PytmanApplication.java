package com.kumbh.pytman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PytmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(PytmanApplication.class, args);
	}

}
